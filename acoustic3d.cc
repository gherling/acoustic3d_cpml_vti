#include <algorithm>
#include <cstdlib>
#include <cstdio>
#include <cmath>
extern "C" {
#include <omp.h>
}
#include "carray.h"

#include <vtkXMLImageDataWriter.h>
#include <vtkXMLImageDataReader.h>
#include <vtkSmartPointer.h>
#include <vtkImageData.h>

// ricker wavelet function
float ricker ( int tStep , //step time
                float dt , //size sample time
                float Fq ) //frequency wavelet
{
   float t,t0,arg;
   t0 =1.0/Fq-dt;               //initial time perturvation
   t = (tStep-1)*dt-t0;         //step time
   arg = M_PI*M_PI*Fq*Fq*t*t;
   return (1-2*arg)*exp(-arg);  //wavelet function
}

void write_vti(char *fname, 
               float   dh,
               const CArray<float> &array) 
{
  int nz = array.Dim(1);
  int ny = array.Dim(2);
  int nx = array.Dim(3);

  vtkSmartPointer<vtkImageData> imageData = vtkSmartPointer<vtkImageData>::New();
  imageData->SetDimensions(nz,ny,nx);
  imageData->SetSpacing(dh,dh,dh);
  imageData->SetOrigin(0,0,0);
  imageData->AllocateScalars(VTK_UNSIGNED_CHAR, 1);

  float norval = 0;
  float maxval = array.max(); //std::valarray::max  C++11
  //https://stackoverflow.com/questions/20676988/vtkimagedata-from-3rd-party-structure  
  unsigned char* voxel=NULL;
  voxel = static_cast<unsigned char*>(imageData->GetScalarPointer());
  
  // Converting from <<float>> to <<unsigned char>>
  for (int i=0; i<array.size(); i++) {
    norval=array[i]/maxval;
    voxel[i] = (int)(127*(norval+1));
  }

  vtkSmartPointer<vtkXMLImageDataWriter> 
  writer = vtkSmartPointer<vtkXMLImageDataWriter>::New();
  writer->SetFileName(fname);
  writer->SetInputData(imageData);
  writer->Write();
}

//  PML damping coefficient vector
void cpml_coeff(size_t PML, //PML zone bundary
                float vmax, //vmax of media
                float R,    //PML parameter atenuation
                float dt,   //time step size
                float dh,   //cell-grid size
                CArray<float> &a, //PML coefficient damping arrays
                CArray<float> &b) //PML coefficient damping arrays
{
  size_t N = a.size();
  float L=PML*dh;
  float d0 = -4*vmax*logf(R)/L;
  float sigma;
  for (size_t i=0; i<N; i++){
    if(i<PML){
     sigma = d0*pow((PML-i)*dh/L,4);
      b[i] = exp(-sigma*dt);
      a[i] = b[i]-1;
    }else if (i >= N-PML){
      b[i] = b[(N-1)-i];   
      a[i] = a[(N-1)-i];
    }else{
      b[i]=1.0;   
      a[i]=0.0;
    }
  }
}


//2th order in time and 4th order in space finite difference
void wavefield(size_t PML,  //PML zone bundary
               float dh,    //grid size 
               float dt,    //time step size
               const CArray<float> &az,   //damping coefficient in x
               const CArray<float> &ay,   //damping coefficient in x
               const CArray<float> &ax,   //damping coefficient in z
               const CArray<float> &bz,   //damping coefficient in z
               const CArray<float> &by,   //damping coefficient in x
               const CArray<float> &bx,   //damping coefficient in x
               const CArray<float> &vel,  //px-wave velocity
               CArray<float> &phiz, //cpml auxiliar wavefields
               CArray<float> &phiy, //cpml auxiliar wavefields
               CArray<float> &phix, //cpml auxiliar wavefields
               CArray<float> &etaz, //cpml auxiliar wavefields
               CArray<float> &etay, //cpml auxiliar wavefields
               CArray<float> &etax, //cpml auxiliar wavefields
               CArray<float> &p,    //p-field current
               CArray<float> &p1)   //p-field next
{
  size_t i,j,k,Ix,Iy,Iz;
  size_t nz = p.Dim(1);
  size_t ny = p.Dim(2);
  size_t nx = p.Dim(3);
  float C2;
  float k0 = -5/2.;
  float k1 =  4/3.;
  float k2 = -1/12.;

  float DzP,DxP,DyP,DzzP,DyyP,DxxP,Dzphiz,Dyphiy,Dxphix;
  
  //update acoustic field step by step in time
#pragma omp parallel for default(shared) \
private(i,j,k,C2,DzP,DyP,DxP,DzzP,DyyP,DxxP,Dzphiz,Dyphiy,Dxphix)
  for (i=2; i<nx-2; i++){
    for (j=2; j<ny-2; j++){
      for (k=2; k<nz-2; k++){
        
        C2 = vel(k,j,i)*dt;
        C2 = C2*C2;

        DzzP= (k2*( p(k+2,j,i) + p(k-2,j,i) ) +
               k1*( p(k+1,j,i) + p(k-1,j,i) ) +
               k0*p(k,j,i))/(dh*dh);

        DyyP= (k1*( p(k,j+1,i) + p(k,j-1,i) ) +
               k2*( p(k,j+2,i) + p(k,j-2,i) ) +
               k0*p(k,j,i))/(dh*dh);     

        DxxP= (k2*( p(k,j,i+2) + p(k,j,i-2) ) +
               k1*( p(k,j,i+1) + p(k,j,i-1) ) +
               k0*p(k,j,i))/(dh*dh);

        // CPML implementation in z 
        if(k<PML || k>nz-PML) {
          
          Iz = (k<PML? k:k-nz+2*PML);

          DzP = (1.0/dh)*(  (2/3.)*( p(k+1,j,i) - p(k-1,j,i) ) +
                           (1/12.)*( p(k-2,j,i) - p(k+2,j,i) ) );

          phiz(Iz,j,i) = bz[k]*phiz(Iz,j,i) + az[k]*DzP;

          Dzphiz = (1.0/dh)*(  (2/3.)*( phiz(Iz+1,j,i) - phiz(Iz-1,j,i) ) +
                              (1/12.)*( phiz(Iz-2,j,i) - phiz(Iz+2,j,i) ) );

          etaz(Iz,j,i) = bz[k]*etaz(Iz,j,i) + az[k]*( DzzP + Dzphiz );

          DzzP += etaz(Iz,k,i) + Dzphiz;
        }      

        // CPML implementation in y
        if(j<PML || j>ny-PML) {
          
          Iy = (j<PML? j:j-ny+2*PML);

          DyP = (1.0/dh)*(  (2/3.)*( p(k,j+1,i) - p(k,j-1,i) ) +
                           (1/12.)*( p(k,j-2,i) - p(k,j+2,i) ) );
        
          phiy(k,Iy,i) = by[j]*phiy(k,Iy,i) + ay[j]*DyP;

          Dyphiy = (1.0/dh)*(  (2/3.)*( phiy(k,Iy+1,i) - phiy(k,Iy-1,i) ) +
                              (1/12.)*( phiy(k,Iy-2,i) - phiy(k,Iy+2,i) ) );

          etay(k,Iy,i) = by[j]*etay(k,Iy,i) + ay[j]*( DyyP + Dyphiy );

          DyyP += etay(k,Iy,i) + Dyphiy;
        }

        // CPML implementation in x
        if(i<PML || i>nx-PML) {
          
          Ix = (i<PML? i:i-nx+2*PML);

          DxP = (1.0/dh)*(  (2/3.)*( p(k,j,i+1)-p(k,j,i-1) ) +
                           (1/12.)*( p(k,j,i-2)-p(k,j,i+2) ) );

          phix(k,j,Ix) = bx[i]*phix(k,j,Ix) + ax[i]*DxP;

        
          Dxphix = (1.0/dh)*(  (2/3.)*( phix(k,j,Ix+1)-phix(k,j,Ix-1) ) +
                              (1/12.)*( phix(k,j,Ix-2)-phix(k,j,Ix+2) ) );

          etax(k,j,Ix) = bx[i]*etax(k,j,Ix) + ax[i]*( DxxP + Dxphix );

          DxxP += etax(k,j,Ix) + Dxphix;
        }

        // 3D finite difference wavefield solution
        p1(k,j,i) = 2*p(k,j,i) - p1(k,j,i) + C2*( DzzP + DyyP +DxxP );

      } //end k-loop
    } //end j-loop
  }//end i-loop

} //end wavefield function



int main(int argc, char* argv[]){
   int i,j,k,tstep;
   int const Tout = 1600;
   int const Nz = 401;
   int const Ny = 302;
   int const Nx = 303;
   int const PML = 45; 
   char namefile[50];     // name's files vti
   float const R = 5e-4;  // PML parameter atenuation
   float  dh,dt,G,cmax,cmin,Fq,x,y,z,r;

   double iStart;
   double iElaps;
   
   /* Lines, Slawinski, and Bording (1999) */
   /* A recipe for stability analysis of   */
   /* finite-difference wave-equation      */ 
   /* computations. Geophysics 64, 967–969 */

   G = 1/2.0;      /* 3d stability condiction            */
   dh = 0.01;      /* cell size [km]                     */
   cmin=1.48;      /* minimum acoustic vel. media [km/s] */
   cmax=3.0;       /* maximum acoustic vel. media [km/s] */

   Fq=cmin/(9*dh);  /*twenty points by wavelengh*/
   //dt=G*dh/cmax;     /*time step size [s]*/
   dt = 0.0012;

   //fprintf(stdout,"--%0.7f--",dt);

   //    CArray<T> (n1,n2,n3);  
   CArray<float> p1(Nz,Ny,Nx);
   CArray<float> p2(Nz,Ny,Nx);
   CArray<float>  c(Nz,Ny,Nx);

   int iZ = Nz/2;
   int iY = Ny/2;
   int iX = Nx/2;
   for( i=0; i<Nx; i++ ){
     x = (i-iX)*dh;
      for( j=0; j<Ny; j++ ){
        y = (j-iY)*dh;
         for ( k=0; k<Nz; k++ ){     
           z = (k-iZ)*dh;
           r = sqrt(z*z + y*y + x*x);
           if ( r < 40*dh )
             c(k,j,i) = cmin;
           else
             c(k,j,i) = cmax;           
         }
      }
   }

   //CPML coefficient damping arrays
   CArray<float> az(Nz),ay(Ny),ax(Nx); 
   CArray<float> bz(Nz),by(Ny),bx(Nx);

   cpml_coeff(PML,cmax,R,dt,dh,az,bz);
   cpml_coeff(PML,cmax,R,dt,dh,ay,by);
   cpml_coeff(PML,cmax,R,dt,dh,ax,bx);

   //cpml auxiliar fields
   CArray<float> phiz(2*PML,Ny,Nx);
   CArray<float> etaz(2*PML,Ny,Nx);
   
   CArray<float> phiy(Nz,2*PML,Nx);
   CArray<float> etay(Nz,2*PML,Nx);

   CArray<float> phix(Nz,Ny,2*PML);
   CArray<float> etax(Nz,Ny,2*PML);

   ////////////////////////////////////
   for (tstep=1; tstep<Tout; tstep++){

     //call wavefield function
     wavefield(PML,dh,dt,az,ay,ax,bz,by,bx,c,phiz,phiy,phix,etaz,etay,etax,p1,p2);

     // adding step plane perturvation 
     float pulse =  ricker(tstep-1,dt,Fq);
     for (i=0; i<Nx; i++){
       for (j=0; j<Ny; j++){
         p2(PML-10,j,i) += 10*pulse;
       }
     }
      
     // Swap field values (update fields) 
     p1.swap(p2); //std::valarray::swap  C++11
      
     if(tstep%2==0){ 
        fprintf(stdout,"%d/%d\n",tstep,Tout);
        sprintf(namefile,"../vti/field-%d.vti", tstep);
        write_vti(namefile,dh,p2 );
     }
   }
   ////////////////////////////////////

   //write_vti( (char*)"velo.vti",dh, c );

   return(0);
}
