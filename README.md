# acoustic3D_cpml_vti

Export VTI (Visualization Toolkit Images, VTK) files for RSF data (ahay.org).   The VTI file created can be loaded into ParaView using the  File -> Open menu option.

## How to compile

To compile the project, simply place yourself into the root directory and run
make.
```
cd $(acoustic3D_cpml_vti)
git remote add origin https://gitlab.com/gherling/acoustic3d_cpml_vti.git
git branch -M main
git push -uf origin main
mkdir build
cd build
cmake ..
make
make install
```
## License

This work is licensed under the GNU General Public License (GPL) v3.0. See the
`LICENSE` file for more details.

## Author

You can contact me to email adress:
* Herling Gonzalez Alvarez: <gherling@gmail.com>
