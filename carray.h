#ifndef CARRAY_H
#define CARRAY_H

#include <valarray>

template <typename T> 
class CArray : public std::valarray <T>
{
  size_t n1,n2,n3;
  public:
  CArray(size_t l, size_t m=1, size_t n=1) : std::valarray <T> (l*m*n){
    n1=l; n2=m; n3=n;
  }
  CArray(const CArray<T>&) = delete;
  CArray& operator=(const CArray<T>&) = delete;

  // Row major order in data array class
  T operator()(size_t i, size_t j=0, size_t k=0) const {
    return (*this)[k*n2*n1+j*n1+i]; // reading
  }

  // Row major order in data array class
  T& operator()(size_t i, size_t j=0, size_t k=0) {
    return (*this)[k*n2*n1+j*n1+i]; // writing
  }

  size_t Dim(size_t n) const {
    return (n>2)? n3 : ((n<2)? n1 : n2);
  }
};

#endif //carray.h
